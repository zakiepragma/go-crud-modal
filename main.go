package main

import (
	"go-crud-modal/controllers/asatidzcontroller"
	"net/http"
)

func main() {
	http.HandleFunc("/", asatidzcontroller.Index)
	http.HandleFunc("/asatidz/get_form", asatidzcontroller.GetForm)

	http.ListenAndServe(":2023", nil)
}
