package entities

type Asatidz struct {
	Id                 int64
	Nama               string
	TempatLahir        string
	TanggalLahir       string
	PendidikanTerakhir string
	Alumni             string
	Alamat             string
}
