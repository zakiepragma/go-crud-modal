package asatidzcontroller

import (
	"bytes"
	"go-crud-modal/entities"
	"go-crud-modal/models/asatidzmodel"
	"html/template"
	"net/http"
)

var asatidzModel = asatidzmodel.New()

func Index(w http.ResponseWriter, r *http.Request) {
	data := map[string]interface{}{
		"data": template.HTML(getData()),
	}
	temp, _ := template.ParseFiles("views/asatidz/index.html")
	temp.Execute(w, data)
}

func getData() string {
	buffer := &bytes.Buffer{}

	temp, _ := template.New("data.html").Funcs(template.FuncMap{
		"increment": func(a, b int) int {
			return a + b
		},
	}).ParseFiles("views/asatidz/data.html")

	var asatidz []entities.Asatidz
	err := asatidzModel.FindAll(&asatidz)
	if err != nil {
		panic(err)
	}

	data := map[string]interface{}{
		"asatidz": asatidz,
	}

	temp.ExecuteTemplate(buffer, "data.html", data)

	return buffer.String()
}

func GetForm(w http.ResponseWriter, r *http.Request) {
	temp, _ := template.ParseFiles("views/asatidz/form.html")
	temp.Execute(w, nil)
}
