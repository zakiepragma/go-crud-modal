package asatidzmodel

import (
	"database/sql"
	"go-crud-modal/config"
	"go-crud-modal/entities"
)

type AsatidzModel struct {
	db *sql.DB
}

func New() *AsatidzModel {
	db, err := config.DBConnection()
	if err != nil {
		panic(err)
	}

	return &AsatidzModel{db: db}
}

func (a *AsatidzModel) FindAll(asatidz *[]entities.Asatidz) error {
	rows, err := a.db.Query("select * from asatidz")
	if err != nil {
		return err
	}

	defer rows.Close()

	for rows.Next() {
		var data entities.Asatidz
		rows.Scan(&data.Id,
			&data.Nama,
			&data.TempatLahir,
			&data.TanggalLahir,
			&data.PendidikanTerakhir,
			&data.Alumni,
			&data.Alamat)

		*asatidz = append(*asatidz, data)
	}
	return nil
}
